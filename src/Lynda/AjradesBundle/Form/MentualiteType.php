<?php

namespace Lynda\AjradesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MentualiteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'hidden')
            ->add('mois','text',array ('attr' => array('disabled' => 'true','class' => 'form-control')))
            ->add('annee','text',array ('attr' => array('disabled' => 'true','class' => 'form-control')))
            ->add('membre', new MembreMentualiteType())
            ->add('montant','integer',array (
                                        'attr' => array(
                                            'class' => 'form-control',
                                            'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57')))     
            ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lynda\AjradesBundle\Entity\Mentualite'         
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lynda_ajradesbundle_mentualite';
    }
}
