<?php

namespace Lynda\AjradesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MembreType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'hidden')
            ->add('lastname','text',array ('attr' => array('class' => 'form-control')))
            ->add('firstname','text',array ('attr' => array('class' => 'form-control')))
            ->add('phonenumber','text',array (
                                            'attr' => array(
                                            'class' => 'form-control',
                                            'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57',
                                            'maxlength' =>'8'
                                            ))) 
            ->add('secteur',null,array ('attr' => array('class' => 'form-control')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lynda\AjradesBundle\Entity\Membre'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lynda_ajradesbundle_membre';
    }
}
