<?php

namespace Lynda\AjradesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjetType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text',array ('attr' => array('class' => 'form-control')))
            ->add('description','text',array ('attr' => array('class' => 'form-control')))
            ->add('datedebut',null,array ('attr' => array('class' => 'form-control')))
            ->add('datefin',null,array ('attr' => array('class' => 'form-control')))
            ->add('budget','integer',array (
                                        'attr' => array(
                                            'class' => 'form-control',
                                            'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lynda\AjradesBundle\Entity\Projet'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lynda_ajradesbundle_projet';
    }
}
