<?php

namespace Lynda\AjradesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Membre
 */
class Membre
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $phonenumber;

    /**
     * @var \Lynda\AjradesBundle\Entity\Secteur
     */
    private $secteur;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id=$id;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Membre
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Membre
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set phonenumber
     *
     * @param string $phonenumber
     * @return Membre
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    /**
     * Get phonenumber
     *
     * @return string 
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * Set secteur
     *
     * @param \Lynda\AjradesBundle\Entity\Secteur $secteur
     * @return Membre
     */
    public function setSecteur(\Lynda\AjradesBundle\Entity\Secteur $secteur = null)
    {
        $this->secteur = $secteur;

        return $this;
    }

    /**
     * Get secteur
     *
     * @return \Lynda\AjradesBundle\Entity\Secteur 
     */
    public function getSecteur()
    {
        return $this->secteur;
    }

    public function __toString()
    {
        return $this->lastname .' '. $this->firstname;
    }
}
