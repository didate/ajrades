<?php

namespace Lynda\AjradesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mentualite
 */
class Mentualite
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $mois;

    /**
     * @var integer
     */
    private $annee;

    /**
     * @var integer
     */
    private $montant;

    /**
     * @var \Lynda\AjradesBundle\Entity\Membre
     */
    private $membre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    /**
     * Set mois
     *
     * @param integer $mois
     * @return Mentualite
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return integer 
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     * @return Mentualite
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return integer 
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set montant
     *
     * @param integer $montant
     * @return Mentualite
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return integer 
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set membre
     *
     * @param \Lynda\AjradesBundle\Entity\Membre $membre
     * @return Mentualite
     */
    public function setMembre(\Lynda\AjradesBundle\Entity\Membre $membre = null)
    {
        $this->membre = $membre;

        return $this;
    }

    /**
     * Get membre
     *
     * @return \Lynda\AjradesBundle\Entity\Membre 
     */
    public function getMembre()
    {
        return $this->membre;
    }
}
