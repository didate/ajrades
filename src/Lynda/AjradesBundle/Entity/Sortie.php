<?php

namespace Lynda\AjradesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sortie
 */
class Sortie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $Motif;

    /**
     * @var \DateTime
     */
    private $datetime;

    /**
     * @var integer
     */
    private $Montant;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Motif
     *
     * @param string $motif
     * @return Sortie
     */
    public function setMotif($motif)
    {
        $this->Motif = $motif;

        return $this;
    }

    /**
     * Get Motif
     *
     * @return string 
     */
    public function getMotif()
    {
        return $this->Motif;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Sortie
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
   


    /**
     * Set Montant
     *
     * @param integer $montant
     * @return Sortie
     */
    public function setMontant($montant)
    {
        $this->Montant = $montant;

        return $this;
    }

    /**
     * Get Montant
     *
     * @return integer 
     */
    public function getMontant()
    {
        return $this->Montant;
    }
}
