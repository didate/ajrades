<?php

namespace Lynda\AjradesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BudgetDetailController extends Controller
{
    public function indexAction($annee)
    {
    	$em = $this->getDoctrine()->getManager();
        //$em->getRepository('LyndaAjradesBundle:Membre')->getMembreNotInMentualite(5,2015);

        $years = $em->getRepository('LyndaAjradesBundle:Mentualite')->getYears();

        $entities = $em->getRepository('LyndaAjradesBundle:BudgetDetail')->budgetWithYear($annee);
        $montantTotal = $em->getRepository('LyndaAjradesBundle:BudgetDetail')->getMontantTotalByAn($annee);
        return $this->render('LyndaAjradesBundle:BudgetDetail:index.html.twig', array(
        				'entities' => $entities,
        				'totals' => $montantTotal,
                        'years' =>$years));
    }

    private function createAnneeForm(Membre $entity)
    {
        $form = $this->createForm(new MembreType(), $entity, array(
            'action' => $this->generateUrl('ajrades_member_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Create', 'attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

}