<?php

namespace Lynda\AjradesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Lynda\AjradesBundle\Entity\Mentualite;
use Lynda\AjradesBundle\Entity\Membre;
use Lynda\AjradesBundle\Entity\Budget;
use Lynda\AjradesBundle\Entity\BudgetDetail;
use Lynda\AjradesBundle\Form\MentualiteType;

/**
 * Mentualite controller.
 *
 */
class MentualiteController extends Controller
{

    /**
     * Lists all Mentualite entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('LyndaAjradesBundle:Mentualite')
                        ->findBy(array('mois' => date('m'),'annee' => date('Y')),array('montant'=>'ASC'))
        ;
        for($i = 0; $i < count($entities); ++$i) { 
            $entities[$i]->setMembre($this->getDoctrine()
                ->getManager()
                    ->getRepository('LyndaAjradesBundle:Membre')
                        ->findOneById($entities[$i]->getMembre()->getId()));
        }
        $caisse = $em->getRepository('LyndaAjradesBundle:Budget')->findAll()[0];
        return $this->render('LyndaAjradesBundle:Mentualite:index.html.twig', array(
            'entities' => $entities,
            'caisse' =>$caisse->getMontant(),
        ));
    }

   
    /**
     * Creates a new Mentualite entity.
     *
     */
    public function createAction(Request $request)
    {

        $form = $this->createCreateForm(new Mentualite());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity = $form->getData();
            $entityMembre =   $em->getRepository('LyndaAjradesBundle:Membre')->find($entity->getMembre()->getId());
            $entity->setMembre($entityMembre);
            $budget = $em->getRepository('LyndaAjradesBundle:Budget')->incrementBudget($entity->getMontant());

            if(!$budget)
            {
                $budget = new Budget();
                $budget->setMontant($entity->getMontant());
            }

            $em->persist($entity);
            $em->persist($budget);
            $em->flush();

            return $this->redirect($this->generateUrl('mentualite_show', array('id' => $entity->getId())));
        }

        return $this->render('LyndaAjradesBundle:Mentualite:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Mentualite entity.
     *
     * @param Mentualite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Mentualite $entity)
    {
        $form = $this->createForm(new MentualiteType(), $entity, array(
            'action' => $this->generateUrl('mentualite_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

   
     public function newAction($id)
    {
        $entity = new Mentualite();
        $entity->setMois(date("m"));
        $entity->setAnnee(date("Y"));
        $em = $this->getDoctrine()->getManager();
        $entityMembre =   $em->getRepository('LyndaAjradesBundle:Membre')->find($id);
        $entity->setMembre($entityMembre);
        $form   = $this->createCreateForm($entity);
        return $this->render('LyndaAjradesBundle:Mentualite:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    public function monthCreateAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $em = $this->getDoctrine()->getManager();
        $entityMembre =   $em->getRepository('LyndaAjradesBundle:Membre')->findAll();
        foreach ($entityMembre as $membre) {
            //check si le membre n'existe pas pour ce mois.
            $mens_ = $em->getRepository('LyndaAjradesBundle:Mentualite')
                            ->getMentualiteByMembre($membre->getId(),date('m'),date('Y'));
            if(!$mens_)
            {
                $mens = new Mentualite();
                $mens->setAnnee(date('Y'));
                $mens->setMois(date('m'));
                $mens->setMembre($membre);
                $mens->setMontant(0);
                $em->persist($mens);
                $em->flush(); 
            }
            
        }
        $caisse = $em->getRepository('LyndaAjradesBundle:Budget')->findAll()[0];
        return $this->redirect($this->generateUrl('mentualite',array('caisse'=>$caisse)));
    }

    /**
     * Finds and displays a Mentualite entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Mentualite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mentualite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LyndaAjradesBundle:Mentualite:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Mentualite entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Mentualite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mentualite entity.');
        }

        $editForm = $this->createEditForm($entity);
        

        return $this->render('LyndaAjradesBundle:Mentualite:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
      
        ));
    }

    /**
    * Creates a form to edit a Mentualite entity.
    *
    * @param Mentualite $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Mentualite $entity)
    {
       
        $form = $this->createForm(new MentualiteType(), $entity, array(
            'action' => $this->generateUrl('mentualite_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing Mentualite entity.
     *
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createEditForm(new Mentualite());
        $editForm->handleRequest($request);
        $entity =  $editForm->getData(); // Récupération des données du formulaire
        if (!$editForm->isValid()) {
            return $this->redirect($this->generateUrl('mentualite_edit', array('id' => $entity->getId())));
        }

        $_entity = $em->getRepository('LyndaAjradesBundle:Mentualite')->find($entity->getId());
        
        if (!$entity) {
            throw $this->createNotFoundException('Impossible de trouver la matualité à modifier.');
        }

        $budget = $em->getRepository('LyndaAjradesBundle:Budget')->incrementBudget(-$_entity->getMontant());

        $_entity->setMontant($entity->getMontant());
        $budget = $em->getRepository('LyndaAjradesBundle:Budget')->incrementBudget($_entity->getMontant());

        if(!$budget)
        {
            $budget = new Budget();
            $budget->setMontant($_entity->getMontant());
        }

        $budgetDetail = $em->getRepository('LyndaAjradesBundle:BudgetDetail')
                                ->incrementBudgetDetail(
                                    $_entity->getMois(),
                                    $_entity->getAnnee(),
                                    $_entity->getMontant()
                                );

        if(!$budgetDetail)
        {
            $budgetDetail = new BudgetDetail();
            $budgetDetail->setMontant($_entity->getMontant());
            $budgetDetail->setMois($_entity->getMois());
            $budgetDetail->setAnnee($_entity->getAnnee());
        }
        $em->persist($budget);
        $em->persist($budgetDetail);
        $em->flush();
        //$caisse = $em->getRepository('LyndaAjradesBundle:Budget')->findAll()[0];
        return $this->redirect($this->generateUrl('dettes_show', array(
            //'caisse'=>$caisse
            'annee' => $_entity->getAnnee(),
            'id' => $_entity->getMembre()->getId(),
        )));
    }

    /**
     * 
     *les dettes
     */
    public function detteAction($annee,$id)
    {

        /*$defaultData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('Annee', 'integer')
            ->add('show', 'submit')
            ->getForm();*/

        $em = $this->getDoctrine()->getManager();

        $years = $em->getRepository('LyndaAjradesBundle:Mentualite')->getYears();       
        $entities = $em->getRepository('LyndaAjradesBundle:Mentualite')
                            ->getMentualiteByMembreAndMontant($id,0,$annee);
       
        
        return $this->render('LyndaAjradesBundle:Mentualite:dette.html.twig', array(
            'entities' => $entities, 
            'years' =>$years, 
            'currentYear' => $annee,
            'id' =>$id
        ));
    }

     /**
     * 
     *les dettes
     */
    public function dettesAction()
    {

        /*$defaultData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('Annee', 'integer')
            ->add('show', 'submit')
            ->getForm();*/
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LyndaAjradesBundle:Mentualite')
                                ->getNbMonthDette();
        
        return $this->render('LyndaAjradesBundle:Mentualite:dettes.html.twig', array(
            'entities' => $entities,
        ));
    }
}
