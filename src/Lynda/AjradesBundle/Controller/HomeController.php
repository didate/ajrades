<?php

namespace Lynda\AjradesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



/**
 * Home controller.
 *
 */
class HomeController extends Controller
{

    
    public function indexAction()
    {
        return $this->render('LyndaAjradesBundle:Home:index.html.twig');
    }  
}
