<?php

namespace Lynda\AjradesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Lynda\AjradesBundle\Entity\Sortie;
use Lynda\AjradesBundle\Form\SortieType;
use \DateTime;
/**
 * Sortie controller.
 *
 */
class SortieController extends Controller
{

    /**
     * Lists all Sortie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LyndaAjradesBundle:Sortie')->findAll();

        return $this->render('LyndaAjradesBundle:Sortie:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Sortie entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Sortie();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setDatetime(new Datetime());
            $budget = $em->getRepository('LyndaAjradesBundle:Budget')->incrementBudget(-$entity->getMontant());
            if(!$budget)
            {
                $budget = new Budget();
                $budget->setMontant(-$entity->getMontant());
            }

            
            $em->persist($budget);
            $em->persist($entity);
            $em->flush();
            $entities = $em->getRepository('LyndaAjradesBundle:Sortie')->findAll();
            return $this->redirect($this->generateUrl('sortie', array('entities' =>  $entities)));
        }

        return $this->render('LyndaAjradesBundle:Sortie:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Sortie entity.
     *
     * @param Sortie $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sortie $entity)
    {
        $form = $this->createForm(new SortieType(), $entity, array(
            'action' => $this->generateUrl('sortie_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create','attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Displays a form to create a new Sortie entity.
     *
     */
    public function newAction()
    {
        $entity = new Sortie();
        $form   = $this->createCreateForm($entity);

        return $this->render('LyndaAjradesBundle:Sortie:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Sortie entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Sortie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sortie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LyndaAjradesBundle:Sortie:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Sortie entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Sortie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sortie entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LyndaAjradesBundle:Sortie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Sortie entity.
    *
    * @param Sortie $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Sortie $entity)
    {
        $form = $this->createForm(new SortieType(), $entity, array(
            'action' => $this->generateUrl('sortie_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing Sortie entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Sortie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sortie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setDatetime(new Datetime());
            $em->flush();

            $entities = $em->getRepository('LyndaAjradesBundle:Sortie')->findAll();
            return $this->redirect($this->generateUrl('sortie', array('entities' =>  $entities)));
        }

        return $this->render('LyndaAjradesBundle:Sortie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Sortie entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LyndaAjradesBundle:Sortie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sortie entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sortie'));
    }

    /**
     * Creates a form to delete a Sortie entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sortie_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
