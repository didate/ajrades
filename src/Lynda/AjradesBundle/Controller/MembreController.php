<?php

namespace Lynda\AjradesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Lynda\AjradesBundle\Entity\Membre;
use Lynda\AjradesBundle\Entity\Budget;
use Lynda\AjradesBundle\Form\MembreType;

/**
 * Membre controller.
 *
 */
class MembreController extends Controller
{

    /**
     * Lists all Membre entities.
     *
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        
        //$entities = $em->getRepository('LyndaAjradesBundle:Membre')->findAll();
        $entities =$em->getRepository('LyndaAjradesBundle:Membre')->getMembres(10,$page);
        $caisse = $em->getRepository('LyndaAjradesBundle:Budget')->findAll()[0];
        
        return $this->render('LyndaAjradesBundle:Membre:index.html.twig', array(
            'entities' => $entities, 
            'caisse' =>$caisse->getMontant(),
            'page'       => $page,
            'nombrePage' => ceil(count($entities)/10)
        ));
    }
    /**
     * Creates a new Membre entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Membre();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();        
            return $this->redirect($this->generateUrl('ajrades_member', array(
                'page'       => 1,
            )));

        }

        return $this->render('LyndaAjradesBundle:Membre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Membre entity.
     *
     * @param Membre $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Membre $entity)
    {
        $form = $this->createForm(new MembreType(), $entity, array(
            'action' => $this->generateUrl('ajrades_member_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Create', 'attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Displays a form to create a new Membre entity.
     *
     */
    public function newAction()
    {
        $entity = new Membre();
        $form   = $this->createCreateForm($entity);

        return $this->render('LyndaAjradesBundle:Membre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Membre entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Membre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Membre entity.');
        }


        return $this->render('LyndaAjradesBundle:Membre:show.html.twig', array(
            'entity'      => $entity,
           
        ));
    }

    /**
     * Displays a form to edit an existing Membre entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Membre')->find($id);

       
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Membre entity.');
        }

        $editForm = $this->createEditForm($entity);


        return $this->render('LyndaAjradesBundle:Membre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Membre entity.
    *
    * @param Membre $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Membre $entity)
    {
        $form = $this->createForm(new MembreType(), $entity, array(
            'action' => $this->generateUrl('ajrades_member_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing Membre entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Membre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Membre entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();          
            return $this->redirect($this->generateUrl('ajrades_member', array(
                'page'       => 1,
            )));
        }

        return $this->render('LyndaAjradesBundle:Membre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
   
}
