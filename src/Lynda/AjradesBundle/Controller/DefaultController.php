<?php

namespace Lynda\AjradesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LyndaAjradesBundle:Default:index.html.twig', array('name' => $name));
    }
}
