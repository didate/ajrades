<?php

namespace Lynda\AjradesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Lynda\AjradesBundle\Entity\Aide;
use Lynda\AjradesBundle\Form\AideType;
use \datetime;

/**
 * Aide controller.
 *
 */
class AideController extends Controller
{

    /**
     * Lists all Aide entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LyndaAjradesBundle:Aide')->findAll();

        return $this->render('LyndaAjradesBundle:Aide:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Aide entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Aide();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $budget = $em->getRepository('LyndaAjradesBundle:Budget')->incrementBudget($entity->getMontant());
            if(!$budget)
            {
                $budget = new Budget();
                $budget->setMontant($entity->getMontant());
            }
            //$entity->setDate(new datetime());
            $em->persist($budget);
            $em->persist($entity);
            $em->flush();


            return $this->redirect($this->generateUrl('lynda_aide'));
        }

        return $this->render('LyndaAjradesBundle:Aide:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Aide entity.
     *
     * @param Aide $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Aide $entity)
    {
        $form = $this->createForm(new AideType(), $entity, array(
            'action' => $this->generateUrl('lynda_aide_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }

    /**
     * Displays a form to create a new Aide entity.
     *
     */
    public function newAction()
    {
        $entity = new Aide();
        $form   = $this->createCreateForm($entity);

        return $this->render('LyndaAjradesBundle:Aide:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Aide entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Aide')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aide entity.');
        }

       

        return $this->render('LyndaAjradesBundle:Aide:show.html.twig', array(
            'entity'      => $entity,
      
        ));
    }

    /**
     * Displays a form to edit an existing Aide entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LyndaAjradesBundle:Aide')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aide entity.');
        }

        $editForm = $this->createEditForm($entity);


        return $this->render('LyndaAjradesBundle:Aide:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Aide entity.
    *
    * @param Aide $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Aide $entity)
    {
        $form = $this->createForm(new AideType(), $entity, array(
            'action' => $this->generateUrl('lynda_aide_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' =>array('class' => 'btn btn-primary btn-xs')));

        return $form;
    }
    /**
     * Edits an existing Aide entity.
     *
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager(); 
        $editForm = $this->createEditForm(new Aide());
        $editForm->handleRequest($request);
        $entity =  $editForm->getData(); // Récupération des données du formulaire
        var_dump($entity);
        if (!$editForm->isValid()) {
            //return $this->redirect($this->generateUrl('lynda_aide_edit', array('id' => $entity->getId())));
        }

        $_entity = $em->getRepository('LyndaAjradesBundle:Aide')->find($entity->getId());
        
        if (!$entity) {
            throw $this->createNotFoundException('Impossible de trouver la matualité à modifier.');
        }

        $budget = $em->getRepository('LyndaAjradesBundle:Budget')->incrementBudget(-$_entity->getMontant());

        $_entity->setMontant($entity->getMontant());
        $budget = $em->getRepository('LyndaAjradesBundle:Budget')->incrementBudget($_entity->getMontant());

        if(!$budget)
        {
            $budget = new Budget();
            $budget->setMontant($_entity->getMontant());
        }

        $em->persist($budget);
        $em->flush();

        return $this->redirect($this->generateUrl('lynda_aide'));
    }
   
}
